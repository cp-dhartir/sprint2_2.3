<?php
session_start();

if (isset($_SESSION['role']) && !empty($_SESSION['role'])) {
    header("location: dashboard.php");
}else{
    header("location: login.php");
}
?>