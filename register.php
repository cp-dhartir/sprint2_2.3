<?php
include('connection.php');

session_start();

if (isset($_SESSION['role']) && !empty($_SESSION['role'])) {
    header("location: dashboard.php");
}

if (isset($_POST['submit'])) {
    $name =  $_POST['name'];
    $email = $_POST['email'];
    $password = md5($_POST['password']);
    $role = $_POST['role'];

    $sql = "INSERT INTO `users`(`name`, `email`, `password`, `role`) 
    VALUES ('$name','$email','$password','$role')";

    if (mysqli_query($conn, $sql)) {
        $_SESSION['role'] = $role;
        header("location: dashboard.php");
    } else {
        echo "ERROR: Hush! Sorry $sql. "
            . mysqli_error($conn);
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Register</title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-4"></div>
            <form class="col-lg-4" action="register.php" method="post">
                <h3 class="mt-5 text-center">Register</h3>
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Firstname" required>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="example@gmail.com" required>
                    <div id="emailTag" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password" required>
                </div>
                <div class="mb-3">
                    <label for="role">Role</label>
                    <div class="d-flex">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="role" id="adminRole" value="admin" checked>
                            <label class="form-check-label" for="adminRole">
                                Admin
                            </label>
                        </div>
                        <div class="form-check ms-5">
                            <input class="form-check-input" type="radio" name="role" id="userRole" value="user">
                            <label class="form-check-label" for="userRole">
                                User
                            </label>
                        </div>
                    </div>
                </div>
                <!-- Register buttons -->
                <div class="text-center">
                    <p>Already a member? <a href="login.php">Login</a></p>
                </div>
                <div class="text-center">
                    <input type="submit" class="btn btn-primary px-5" name="submit" value="Submit">
                </div>
            </form>
            <div class="col-lg-4"></div>
        </div>
    </div>
</body>
<?php
$conn->close();
?>
</html>