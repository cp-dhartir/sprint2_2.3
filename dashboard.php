<?php
include('connection.php');

session_start();

if (isset($_SESSION['role']) && !empty($_SESSION['role'])) {
    if ($_SESSION['role'] == 'admin') {
        $sql = "SELECT * FROM posts ORDER BY id DESC";
    } else {
        $sql = "SELECT * FROM posts WHERE visible = 1 ORDER BY id DESC";
    }

    $row = mysqli_query($conn, $sql);
} else {
    header("location: login.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Dashboard</title>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="">
                <h3>Dashboard</h3>
            </a>
            <div>
                <?php if ($_SESSION['role'] == 'admin') { ?>
                    <a href="createPost.php" class="btn btn-outline-primary">+ Create Post</a>
                <?php } ?>
                <a href="logout.php" class="btn btn-outline-danger">Logout</a>
            </div>
        </div>
    </nav>
    <div class="container">
        <?php foreach ($row as $value) {
        ?>
            <div class="row border border-black p-2">
                <?php if ($_SESSION['role'] == 'admin') {

                    if ($value['visible'] == '0') { ?>

                        <h5 class="p-2 bg-danger text-white">Non Public</h3>

                        <?php } else { ?>

                            <h5 class="p-2 bg-success text-white">Public</h3>

                        <?php }
                } ?>
                        <div class="col-md-12 p-2">
                            <img src="<?php echo $value['image']; ?>" alt="Image" class="rounded" style="height: 100%;width: 100%;object-fit: cover;">
                        </div>
                        <div class="col-md-12">
                            <h4><?php echo $value['title']; ?> - <?php echo $value['category']; ?></h4>
                            <p><?php echo $value['description']; ?></p>
                            <div class="d-flex justify-content-between">
                                <div class="d-flex">
                                    <h5>Author Name - </h5>
                                    <p class="ms-2"><?php echo $value['author']; ?></p>
                                </div>
                                <div class="d-flex">
                                    <h5>Date - </h5>
                                    <p class="ms-2"><?php echo $value['created_at']; ?></p>
                                </div>
                            </div>
                        </div>
            </div>
        <?php
        }
        ?>
    </div>
</body>

<?php
$conn->close();
?>

</html>