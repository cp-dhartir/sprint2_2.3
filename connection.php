<?php
$servername = "127.0.0.1";
$username = "root";
$password = "root";
$dbname = "practical23";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// sql to create table
// $sql = "CREATE TABLE users (
//   id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//   name VARCHAR(50) NOT NULL,
//   email VARCHAR(50) NOT NULL,
//   password VARCHAR(255) NOT NULL,
//   role VARCHAR(10) NOT NULL
//   )";

// $sql = "CREATE TABLE posts (
//   id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//   title VARCHAR(255) NOT NULL,
//   description VARCHAR(1000) NOT NULL,
//   author VARCHAR(100) NOT NULL,
//   category VARCHAR(100) NOT NULL,
//   image VARCHAR(255) NOT NULL,
//   visible BOOLEAN DEFAULT FALSE NOT NULL,
//   created_at DATE  NOT NULL)";

// if ($conn->query($sql) === TRUE) {
//   echo "Table users created successfully";
// } else {
//   echo "Error creating table: " . $conn->error;
// }
?>