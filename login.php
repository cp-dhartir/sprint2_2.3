<?php
include('connection.php');

session_start();

if (isset($_SESSION['role']) && !empty($_SESSION['role'])) {
    header("location: dashboard.php");
}

if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $password = md5($_POST['password']);

    if (isset($username) && isset($password)) {
        $sql = "SELECT * FROM users WHERE email='$email' and password='$password'";
        $result = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($result);
        $count = mysqli_num_rows($result);

        if ($count == 1) {
            $_SESSION['role'] = $row['role'];
            header("location: dashboard.php");
        } else {
            echo "<center>
                    <h4 class='alert alert-danger'>This Creadentials Do Not Match Over Records</h4>
                </center>";
        }
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Login</title>
</head>

<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-lg-4"></div>
            <form class="col-lg-4" action="login.php" method="post">
                <h3 class="mt-5 text-center">Login</h3>
                <div class="mb-3">
                    <label for="email" class="form-label">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" placeholder="example@gmail.com" required>
                    <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                </div>
                <!-- Register buttons -->
                <div class="text-center">
                    <p>Not a member? <a href="register.php">Register</a></p>
                </div>
                <div class="text-center">
                    <input type="submit" class="btn btn-primary px-5" name="submit" value="Submit">
                </div>
            </form>
            <div class="col-lg-4"></div>
        </div>
    </div>
</body>
<?php
$conn->close();
?>
</html>