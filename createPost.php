<?php
include('connection.php');

session_start();

if (isset($_SESSION['role']) && !empty($_SESSION['role'])) {
} else {
    header("location: login.php");
}

if (isset($_POST['submit'])) {
    $title =  $_POST['title'];
    $description = $_POST['description'];
    $author =  $_POST['author'];
    $category = $_POST['category'];
    $visible = $_POST['visible'];
    $time = date("d-m-Y") . "-" . time();

    //  echo "$title\n $description\n $author\n $category\n $visible\n";

    $target_dir = "uploads/";
    $target_file = $target_dir . $time . '-' . basename($_FILES["image"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

    // Check if image file is a actual image or fake image
    if (isset($_POST["submit"])) {
        $check = getimagesize($_FILES["image"]["tmp_name"]);
        if ($check !== false) {
            // echo "File is an image - " . $check["mime"] . ".<br>";
            $uploadOk = 1;
        } else {
            echo "<div class='alert alert-danger'>File is not an image.</div><br>";
            $uploadOk = 0;
        }
    }

    // Check if file already exists
    if (file_exists($target_file)) {
        echo "<div class='alert alert-danger'>Sorry, file already exists.</div><br>";
        $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["image"]["size"] > 500000) {
        echo "<div class='alert alert-danger'>Sorry, your file is too large.</div><br>";
        $uploadOk = 0;
    }

    // Allow certain file formats
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        echo "<div class='alert alert-danger'>Sorry, only JPG, JPEG & PNG files are allowed.</div><br>";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "<div class='alert alert-danger'>Sorry, your file was not uploaded.</div><br>";
        // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
            $sql = "INSERT INTO `posts`(`title`, `description`, `author`, `category`, `image`, `visible`, `created_at`) 
                    VALUES ('$title', '$description', '$author', '$category', '$target_file','$visible',CURDATE())";

            if (mysqli_query($conn, $sql)) {
                // echo "<div class='alert alert-success'>The file " . htmlspecialchars(basename($_FILES["image"]["name"])) . " has been uploaded.</div><br>";
                // echo "<div class='alert alert-success'>The post has been uploaded.</div><br>";
                header("location: dashboard.php");
            } else {
                echo "ERROR: Hush! Sorry $sql. "
                    . mysqli_error($conn);
            }
        } else {
            echo "<div class='alert alert-danger'>Sorry, there was an error uploading your file.</div><br>";
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Post | Create</title>
</head>

<body>
    <div class="container">
        <h3 class="my-5">Create Post</h3>
        <form action="createPost.php" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-6 mb-4">
                    <label class="form-label" for="title">Title:</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title" required />
                </div>
                <div class="col-md-6 mb-4">
                    <label class="form-label" for="description">Description:</label>
                    <input type="text" class="form-control" id="description" name="description" placeholder="Enter Description" required />
                </div>
                <div class="col-md-6 mb-4">
                    <label class="form-label" for="author">Author:</label>
                    <input type="text" class="form-control" id="author" name="author" placeholder="Enter Author" required />
                </div>
                <div class="col-md-6 mb-4">
                    <label class="form-label" for="category">Category:</label>
                    <input type="text" class="form-control" id="category" name="category" placeholder="Enter Category" required />
                </div>
                <div class="col-md-6 mb-4">
                    <label class="form-label" for="image">Select post image to upload:</label>
                    <input type="file" class="form-control" id="image" name="image" required />
                </div>
                <div class="mb-4">
                    <label for="visible">Publicly Visible</label>
                    <div class="d-flex">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="visible" id="visibleyes" value="1">
                            <label class="form-check-label" for="visibleyes">
                                Yes
                            </label>
                        </div>
                        <div class="form-check ms-5">
                            <input class="form-check-input" type="radio" name="visible" id="visibleno" value="0" checked>
                            <label class="form-check-label" for="visibleno">
                                No
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <input type="submit" class="btn btn-primary" name="submit" value="Upload Post">
        </form>
    </div>
</body>
<?php
$conn->close();
?>
</html>